<?php
//$start = microtime(1);
require __DIR__ . '/../vendor/autoload.php';

$kernel = new \Blow\Foundation\Kernel();
$container = $kernel->getApplication();

$container->add("base.path", __DIR__ . '/../');

$container->add('env.file', 'env');
$container->add('env.format', 'json');

$kernel->handle();

//var_dump(number_format(microtime(1) - $start, 4));