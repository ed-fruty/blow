<?php
/** @var Blow\Foundation\Application $app */

// Path
$app->add('path', 'Blow\Foundation\Path', true)
    ->withArgument($this->app->get('base.path'));


// Config
$app->add('config', 'Blow\Config\Config', true)
    ->withArgument('app');