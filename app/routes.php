<?php

/** @var \Blow\Routing\Router $router */
$router = \Blow\Foundation\Facade::getApplication()->get('router');


$router->addRoute('GET', '/', function($request, $response)
{
    return $response->setContent('Hello world');
});

$router->group(['namespace' => 'Modules\Home\Http\Actions', 'prefix' => 'home'], function($router)
{
    $router->addRoute('GET', '/', function($request, $response)
    {
        return $response->setContent('Hello world');
    });

    $router->action('/form', 'FormAction');
});

$router->action('/middleware', 'Modules\Home\Http\Actions\FormAction', ['GET'], ['auth'] );


$router->addRoute('GET', '/test-request', function(\Blow\Routing\Http\FormRequest $request)
{
    var_dump($request);
}, new \League\Route\Strategy\MethodArgumentStrategy() );