<?php

return [
    'permanentMiddleware' => [
        'Blow\Foundation\Middleware\CsfrTokenMiddleware',
        'Blow\Foundation\Middleware\RouteRequestMiddleware',
    ],

    'middleware' => [
        'auth' => 'Box\Middleware\Auth',
    ],

    'serviceProviders' => [
        'Blow\Routing\RoutingServiceProvider',
        'Box\ServiceProviders\ErrorServiceProvider'
    ],

    'permanentServices' => [

    ]
];