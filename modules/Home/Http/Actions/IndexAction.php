<?php
namespace Modules\Home\Http\Actions;

class IndexAction 
{
    public function __invoke()
    {
        return $this->render('template', []);
    }

    protected function render($template, $data)
    {
        return $this->getContainer()->get('view')->make($template, $data);
    }
}