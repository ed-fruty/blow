<?php
namespace Box;

use Blow\Foundation\Middleware\Middleware;
use League\Route\Http\Exception\NotFoundException;

/**
 * Class Action
 * @package Box
 */
abstract class Action
{
    /**
     * Handle request
     *
     * @return mixed
     */
    public function handle()
    {
        $this->throwNotFoundException();
    }

    /**
     * @param $template
     * @param array $data
     * @return mixed
     */
    protected function render($template, array $data = [])
    {
        return app()->get('view')->render($template, $data, $this);
    }

    /**
     * @throws NotFoundException
     * @param string $message
     */
    protected function throwNotFoundException($message = null)
    {
        throw new NotFoundException($message);
    }

    /**
     * Run middleware
     *
     * @param string|array $middleware
     */
    protected function middleware($middleware)
    {
        Middleware::run($middleware);
    }

    protected function redirect($url, $status = 302, $headers = array())
    {
        return app()->get('request')->redirect($url, $status, $headers);
    }

    protected function redirectBack()
    {
        return app()->get('request')->redirectBack();
    }
}