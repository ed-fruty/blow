<?php
namespace Box\ServiceProviders;

use Blow\Support\AbstractServiceProvider;
use Whoops\Handler\JsonResponseHandler;
use Whoops\Provider\Phalcon\WhoopsServiceProvider;

use \Whoops\Run;
use \Whoops\Handler\PrettyPageHandler;

class ErrorServiceProvider extends AbstractServiceProvider
{

    public function register()
    {
        $whoops = new Run();
        $whoops->pushHandler(new PrettyPageHandler());

        $jsonHandler = new JsonResponseHandler();
        $jsonHandler->onlyForAjaxRequests(true);
        $whoops->pushHandler($jsonHandler);

        $whoops->register();
    }

    public function boot()
    {
        // TODO: Implement boot() method.
    }
}