<?php
namespace Blow\Foundation;

use Blow\Routing\Http\FormRequest;
use League\Container\Container;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Container
 * @package Blow\Container
 */
class Application extends Container
{
    /**
     * @var Container
     */
    private static $instance;

    /**
     *
     */
    public function setAsGlobal()
    {
        static::$instance = $this;
        $this->add('app', $this, true);
        $this->add(get_called_class(), $this, true);
        $this->add('League\Container\ContainerInterface', $this, true);
        Facade::setApplication($this);
    }

    /**
     * @return Container
     */
    public function getInstance()
    {
        return static::$instance;
    }

    function callAction($alias, array $args = [])
    {
        if (is_string($alias) && array_key_exists($alias, $this->callables)) {
            $definition = $this->callables[$alias];

            return $definition($args);
        }

        if (is_callable($alias)) {
            $callable = $this->reflectCallable($alias);
            $args     = $this->resolveCallableArguments($callable, $args);
            foreach ($args as $argument) {
                if ($argument instanceof FormRequest) {
                    $handled = $argument->handle();
                    if ($handled instanceof Response) {
                        return $handled;
                    }
                }
            }

            return call_user_func_array($alias, $args);
        }

        throw new \RuntimeException(
            sprintf('Unable to call callable [%s], does it exist and is it registered with the container?', $alias)
        );
    }
}