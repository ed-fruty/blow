<?php
namespace Blow\Foundation;

use Blow\Support\AbstractServiceProvider;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Kernel
 * @package Blow\Foundation
 */
class Kernel
{
    /**
     * @var bool
     */
    protected static $booted;

    /**
     * @var Application
     */
    protected $app;

    /**
     *
     */
    public function __construct()
    {
        $this->app = $this->newApplication();
    }

    /**
     * @return Application
     */
    protected function newApplication()
    {
        return new Application();
    }

    /**
     * @return Application
     */
    public function getApplication()
    {
        return $this->app;
    }

    /**
     * Handle
     */
    public function handle()
    {
        if (static::$booted) {
            return null;
        }
        $this->boot();
        $this->dispatch();
    }

    /**
     * Boot kernel
     */
    protected function boot()
    {
        $this->app->setAsGlobal();
        $this->coreBootstrap();
        $this->makeApplicationServiceProviders();
        static::$booted = true;
    }


    protected function coreBootstrap()
    {
        if (is_file($filename = $this->app->get('base.path') . '/app/bootstrap.php')) {
            $app = $this->app;
            require $filename;
        }
    }

    protected function makeApplicationServiceProviders()
    {
        $providers = $this->app->get('config')->get('app.serviceProviders');

        /*
         * Register Service Providers
         */
        foreach ($providers as $provider) {
            $this->resolveServiceProvider($provider)->register();
        }

        /*
         * Boot Service Providers
         */
        foreach ($providers as $provider) {
            $this->resolveServiceProvider($provider)->boot();
        }
    }

    /**
     * @param $provider
     * @return AbstractServiceProvider
     */
    protected function resolveServiceProvider($provider)
    {
        $instance = new $provider($this->app);
        if ($instance instanceof AbstractServiceProvider === false) {
            throw new InvalidArgumentException("Provider must extends of AbstractServiceProvider class", 500);
        }
        return $instance;
    }

    protected function dispatch()
    {
        $response = null;
        if ($response = $this->makeMiddlewareResponse()) {
            if ($response instanceof Response !== false) {
                /** @var Response $response */
                $response->send();
            }
        }
    }

    /**
     * Decorate response by middleware
     */
    protected function makeMiddlewareResponse()
    {
        $list = $this->app->get('config')->get('app.permanentMiddleware', []);
        $middleware = null;
        foreach ($list as $el) {
            /** @var \Blow\Foundation\Middleware\Middleware $middleware */
            $middleware = new $el($this->app, $middleware);
        }
        if ($middleware) {
            return $middleware->handle();
        }
        return null;
    }
}