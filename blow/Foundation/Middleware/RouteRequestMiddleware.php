<?php
namespace Blow\Foundation\Middleware;

class RouteRequestMiddleware extends Middleware
{
    public function handle()
    {
        return $this->app->get('router')
            ->getDispatcher()
            ->dispatch($this->request->getMethod(), $this->request->getPathInfo());
    }
}