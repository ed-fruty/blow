<?php
namespace Blow\Foundation\Middleware;

use League\Route\Http\Exception\BadRequestException;

class CsfrTokenMiddleware extends Middleware
{
    public function handle()
    {
        if (strtolower($this->request->getMethod()) !== 'get') {
            $sessionToken = $this->app->get('session')->token();
            $requestToken    = $this->request->get('_token');
            if ($requestToken !== $sessionToken) {
                throw new BadRequestException("Request token don't match with user token", 500);
            }
        }
        return $this->next();
    }
}