<?php
namespace Blow\Foundation\Middleware;

use Blow\Foundation\Application;

abstract class Middleware
{
    protected $app;

    protected $next;

    /**
     * @var \Blow\Routing\Http\Request
     */
    protected $request;

    public function __construct(Application $application, Middleware $middleware = null)
    {
        $this->app = $application;
        $this->next = $middleware;
        $this->request = $application->get('request');
    }

    abstract public function handle();


    protected function next()
    {
        if ($this->next) {
            return $this->next->handle();
        }
        return null;
    }

    public static function run($middleware)
    {
        foreach ((array) $middleware as $item) {
            if (! $className = app()->get('config')->get("app.middleware.{$item}")) {
                throw new \RuntimeException("Undefined middleware {$item}", 500);
            }
            /** @var Middleware $instance */
            $instance = new $className(app());
            if ($instance instanceof Middleware === false) {
                throw new \RuntimeException("Middleware class must extend 'Blow\\Foundation\\Middleware\\Middleware' class", 500);
            }
            $instance->handle();
        }
    }
}