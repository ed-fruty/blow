<?php
namespace Blow\Foundation;

class Path
{
    protected $base;

    protected $dirSep;

    protected $namespaces = [];

    public function __construct($base)
    {
        $this->base = $base;
        $this->dirSep = DIRECTORY_SEPARATOR;
    }

    public function base()
    {
        return $this->base;
    }

    public function get($path)
    {
        return rtrim($this->base, $this->dirSep) . $this->dirSep . ltrim($path, $this->dirSep);
    }

    public function addNamespace($namespace, $path)
    {
        $this->namespaces[$namespace] = $path;
    }

    public function getNamespacePath($namespace)
    {
        return $this->namespaces[$namespace] = $namespace;
    }
}