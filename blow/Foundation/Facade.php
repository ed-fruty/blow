<?php
namespace Blow\Foundation;

class Facade 
{
    private static $application;

    public static function setApplication($application)
    {
        static::$application = $application;
    }

    /**
     * @return \Blow\Foundation\Application
     */
    public static function getApplication()
    {
        return static::$application;
    }
}