<?php
namespace Blow\Config;

use Blow\Foundation\Application;

class Config
{
    protected $app;

    protected $data = [];

    protected $cache = [];

    protected $ext = '.php';

    public function __construct(Application $application)
    {
        $this->app = $application;
    }

    public function get($key, $default = null)
    {
        if (! isset($this->cache[$key])) {
            $this->cache[$key] = $this->getFromStorage($key, $default);
        }
        return $this->cache[$key];
    }

    protected function getFromStorage($key, $default)
    {
        if (strpos($key, '::') !== false) {
            list($namespace, $key) = explode('::', $key);
            $path = $this->app->get('path')->getNamespacePath($namespace);
        } else {
            $path = $this->app->get('path')->get('app/config');
        }
        if (strpos($key, '.') ===  false) {
            return $default;
        }

        $segments = explode('.', $key);
        $filename = rtrim($path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . array_shift($segments) . $this->ext;
        $hash = md5($filename);

        if (! isset($this->data[$hash])) {
            if (! is_file($filename)) {
                throw new \InvalidArgumentException("Configuration file {$filename} not found", 500);
            }
            $this->data[$hash] = require $filename;
        }

        return $this->getItem($this->data[$hash], $key, $segments, $default);
    }

    protected function getItem($storage, $key,  $segments, $default)
    {
        if (isset($storage[$key])) {
            return $storage[$key];
        }
        foreach ($segments as $segment) {
            if (! is_array($storage) || ! isset($storage[$segment])) {
                return $default;
            }
            $storage = $storage[$segment];
        }
        return $storage;
    }
}