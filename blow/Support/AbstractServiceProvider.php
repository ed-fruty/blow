<?php
namespace Blow\Support;

use Blow\Foundation\Application;

abstract class AbstractServiceProvider
{
    protected $app;

    public function __construct(Application $application)
    {
        $this->app = $application;
    }

    abstract public function register();

    abstract public function boot();
}