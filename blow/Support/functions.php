<?php

if (! function_exists('app')) {
    function app()
    {
        return \Blow\Foundation\Facade::getApplication();
    }
}