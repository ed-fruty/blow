<?php
namespace Blow\Routing;

use League\Route\RouteCollection;
use League\Route\Strategy\RequestResponseStrategy;
use FastRoute\RouteCollector;

/**
 * Class Router
 * @package Blow\Routing
 */
class Router extends RouteCollection
{
    /**
     * Named and isolated groups of routes
     *
     * @var array
     */
    protected static $packs = [];

    /**
     * @param $route
     * @param $handler
     * @param array $method
     * @param array|string $middleware
     */
    public function action($route, $handler, $method = ['GET'], $middleware = null)
    {
        $this->addRoute($method, $route, "{$handler}::handle", null, $middleware);
    }

    /**
     * @param string $method
     * @param string $route
     * @param callable|string $handler
     * @param null $strategy
     * @param array|string $middleware
     * @return RouteCollection|void
     */
    public function addRoute($method, $route, $handler, $strategy = null, $middleware = null)
    {
        $strategy = $this->makeStrategySingleton($strategy);
        // parent::addRoute($method, $route, $handler, $strategy);

        // are we running a single strategy for the collection?
        $strategy = (! is_null($this->strategy)) ? $this->strategy : $strategy;

        // if the handler is an anonymous function, we need to store it for later use
        // by the dispatcher, otherwise we just throw the handler string at FastRoute
        if ($handler instanceof \Closure) {
            $callback = $handler;
            $handler  = uniqid('league::route::', true);

            $this->routes[$handler]['callback'] = $callback;
        }

        $this->routes[$handler]['strategy'] = (is_null($strategy)) ? new RequestResponseStrategy : $strategy;
        $this->routes[$handler]['middleware'] = $middleware;

        $route = $this->parseRouteString($route);
        RouteCollector::addRoute($method, $route, $handler);
    }

    /**
     * Builds a dispatcher based on the routes attached to this collection
     *
     * @return \League\Route\Dispatcher
     */
    public function getDispatcher()
    {
        $dispatcher = new Dispatcher($this->container, $this->routes, $this->getData());

        if (! is_null($this->strategy)) {
            $dispatcher->setStrategy($this->strategy);
        }

        return $dispatcher;
    }

    /**
     * @param $strategy
     * @return mixed
     */
    protected function makeStrategySingleton($strategy)
    {
        return $strategy;
    }

    /**
     * @param $route
     * @param $handler
     */
    public function resource($route, $handler)
    {
        //TODO
    }

    /**
     * @param $route
     * @param $handler
     * @param array $methods
     * @param array|string $middleware
     * @param bool $strong
     */
    public function controller($route, $handler, $methods = ['GET', 'POST'], $middleware = null, $strong = false)
    {
        $classMethods = $this->getControllerHandlerMethods($handler);
        $additional = $strong ? "" : "{param:|\/.*}";
        foreach ($classMethods as $item) {
            if (substr($item, 0, 1) === '_') {
                // method name begin from `_`, so it can be magic method, skip it
                continue;
            }
            $this->addRoute($methods, "{$route}/{$item}{$additional}", "{$handler}::{$item}");
        }
        if (in_array('index', $classMethods) !== false) {
            $this->addRoute($methods, "{$route}", "{$handler}::index");
        }
    }

    /**
     * @param $handler
     * @return array
     */
    public function getControllerHandlerMethods($handler)
    {
        return get_class_methods($handler);
    }

    /**
     * @param array $options
     * @param $closure
     */
    public function group(array $options, $closure)
    {
        $collection = new GroupRouter($this->container);
        foreach ($options as $k => $v) {
            $collection->$k = $v;
        }

        $closure($collection);
        foreach ($collection->getList() as $el) {
            $this->addRoute($el['method'], $el['route'], $el['handler'], $el['strategy'], $el['middleware']);
        }

        //var_dump($this->getData());
    }

    /**
     * Collect routes as pack
     *
     * @example $router->pack('module_blog_admin_routes', function($router)
     * {
     *   $router->group(['prefix' => 'blog', 'namespace' => 'Modules\Blog\Http\Actions'], function($router)
     *   {
     *      $router->action('/', 'IndexAction');
     *      $router->action('create', 'CreateAction');
     *      $router->action('edit/{id}', 'EditAction');
     *      $router->action('delete/{id}', 'DeleteAction', 'DELETE');
     *   })
     * })
     * @param $name
     * @param $closure
     */
    public function pack($name, $closure)
    {
        self::$packs[$name][] = [
            'instance' => $this,
            'callback' => $closure
        ];
    }

    /**
     * Unpack routes by name
     *
     * @example $router->group(['prefix' => 'admin', 'middleware' => 'auth'], function($router)
     * {
     *   $router->unpack('module_blog_admin_routes');
     *   $router->unpack('module_products_admin_routes');
     * })
     * @param $name
     */
    public function unPack($name)
    {
        if (isset(self::$packs[$name])) {
            foreach (self::$packs[$name] as $pack) {
                list($instance, $callback) = $pack;
                $callback($instance);
            }
        }
    }
}