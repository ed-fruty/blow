<?php
namespace Blow\Routing;

/**
 * Class GroupRouter
 * @package Blow\Routing
 */
class GroupRouter extends Router
{
    /**
     * @var array
     */
    protected $list = [];

    /**
     * @var string
     */
    public $prefix;

    /**
     * @var string
     */
    public $namespace;

    /**
     * @var array|string
     */
    protected $middleware;

    /**
     * @param string $method
     * @param string $route
     * @param callable|string $handler
     * @param null $strategy
     * @param array|string $middleware
     * @return \League\Route\RouteCollection|void
     */
    public function addRoute($method, $route, $handler, $strategy = null, $middleware = null)
    {
        $this->list[] = [
            'method' => $method,
            'route'  => $this->getRoute($route),
            'handler'=> $this->getHandler($handler),
            'strategy' => $strategy,
            'middleware' => $this->getMiddleware($middleware),
        ];
    }

    /**
     * @return array
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * @param $handler
     * @return array
     */
    public function getControllerHandlerMethods($handler)
    {
        $handler = $this->getHandler($handler);
        return ($handler instanceof \Closure) ? [] : get_class_methods($handler);
    }

    /**
     * @param $route
     * @return string
     */
    protected function getRoute($route)
    {
        if ($this->prefix) {
            $route = ($route === '/') ? '' : '/' . trim($route, '/') ;
            return '/' . trim($this->prefix . $route, '/');
        }
        return $route;
    }

    /**
     * @param $handler
     * @return string
     */
    protected function getHandler($handler)
    {
        if ($this->namespace && $handler instanceof \Closure === false) {
            return rtrim($this->namespace, "\\") . "\\" . $handler;
        }
        return $handler;
    }

    protected function getMiddleware($middleware)
    {
        return array_merge((array) $this->middleware, (array) $middleware);
    }
}