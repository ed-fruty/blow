<?php
namespace Blow\Routing\Http;

class ExampleFormRequest extends FormRequest
{
    protected function rules()
    {
        return [
            'name' => 'required'
        ];
    }

    protected function formSchema($form)
    {
        $form->input('name', 'Name');
    }

    protected function authenticate()
    {
        return true;
    }
}