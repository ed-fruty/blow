<?php
namespace Blow\Routing\Http;
use League\Route\Http\Exception\NotFoundException;

/**
 * Class FormRequest
 * @package Blow\Routing\Http
 */
class FormRequest
{

    /**
     * @var
     */
    protected $form;

    /**
     * @param $method
     * @param $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        return call_user_func_array([app()->get('request'), $method], $args);
    }

    /**
     *
     */
    public function handle()
    {
        if ($this->authenticate() && $this->validate()) {
            return $this->success();
        }
        return $this->fail();
    }

    /**
     * Instance of form request without auto handling validation
     *
     * @param bool
     * @return static
     */
    public static function newInstance()
    {
        return new static();
    }

    /**
     * @return bool
     */
    protected function authenticate()
    {
        return false;
    }

    /**
     * @return mixed
     */
    protected function validate()
    {
        return $this->getValidator()
            ->setRules($this->rules())
            ->setMessages($this->messages())
            ->validate();
    }

    /**
     * @return null
     */
    protected function success()
    {
        return null;
    }

    /**
     * @return mixed
     */
    protected function fail()
    {
        return app()->get('request')->redirectBack();
    }

    /**
     * @return mixed
     */
    protected function getValidator()
    {
        return app()->get('validator');
    }

    /**
     * @return array
     */
    protected function rules()
    {
        return [];
    }

    /**
     * @return array
     */
    protected function messages()
    {
        return [];
    }

    /**
     * @param $form
     */
    protected function formSchema($form)
    {

    }

    /**
     * @return mixed
     */
    public function getForm()
    {
        if (! $this->form) {
            $this->form = $this->createForm();
            $this->formSchema($this->form);
        }
        return $this->form;
    }

    /**
     * @return mixed
     */
    protected function createForm()
    {
        return app()->get('form');
    }
}