<?php
namespace Blow\Routing\Http;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request as ParentRequest;

class Request extends ParentRequest
{
    public function redirect($url, $status = 302, $headers = array())
    {
        return new RedirectResponse($url, $status, $headers);
    }

    public function redirectBack()
    {
        $url = $this->headers->get('referer') ?: '/';
        return $this->redirect($url);
    }
}