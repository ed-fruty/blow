<?php
namespace Blow\Routing;

use Blow\Routing\Http\Request;
use Blow\Support\AbstractServiceProvider;

class RoutingServiceProvider extends AbstractServiceProvider
{

    public function register()
    {
        $this->app->add('router', 'Blow\Routing\Router', true);
        $this->app->add('request', Request::createFromGlobals(), true);
    }

    public function boot()
    {
        if (! is_file($filename = $this->app->get('base.path') . 'app/routes.php')) {
            throw new \RuntimeException("{$filename} not found", 500);
        }
        require_once $filename;
    }
}