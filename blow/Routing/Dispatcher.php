<?php
namespace Blow\Routing;

use League\Route\Dispatcher as LeagueDispatcher;
use League\Route\Strategy\StrategyInterface;
use Closure;
use RuntimeException;
use League\Container\ContainerAwareInterface;
use FastRoute\Dispatcher as FastRoute;
use Blow\Foundation\Middleware\Middleware;
use FastRoute\Dispatcher\GroupCountBased as ParentDispatcher;

class Dispatcher extends LeagueDispatcher
{
    /**
     * Match and dispatch a route matching the given http method and uri
     *
     * @param  string $method
     * @param  string $uri
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dispatch($method, $uri)
    {

        $match = ParentDispatcher::dispatch($method, $uri);
        if ($match[0] === FastRoute::NOT_FOUND) {
            return $this->handleNotFound();
        }

        if ($match[0] === FastRoute::METHOD_NOT_ALLOWED) {
            $allowed  = (array) $match[1];
            return $this->handleNotAllowed($allowed);
        }

        $handler  = (isset($this->routes[$match[1]]['callback'])) ? $this->routes[$match[1]]['callback'] : $match[1];
        $strategy = $this->routes[$match[1]]['strategy'];
        $middleware = $this->routes[$match[1]]['middleware'];
        $vars     = (array) $match[2];


        return $this->handleFound($handler, $strategy, $middleware, $vars);
    }

    /**
     * Handle dispatching of a found route
     *
     * @param  string|\Closure $handler
     * @param  \League\Route\Strategy\StrategyInterface $strategy
     * @param array|string $middleware
     * @param  array $vars
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws RuntimeException
     */
    protected function handleFound($handler, StrategyInterface $strategy, $middleware, array $vars)
    {
        if (is_null($this->getStrategy())) {
            $this->setStrategy($strategy);
        }

        $controller = null;

        // figure out what the controller is
        if (($handler instanceof Closure) || (is_string($handler) && is_callable($handler))) {
            $controller = $handler;
        }

        if (is_string($handler) && strpos($handler, '::') !== false) {
            $controller = explode('::', $handler);
        }

        // if controller method wasn't specified, throw exception.
        if (! $controller) {
            throw new RuntimeException('A class method must be provided as a controller. ClassName::methodName');
        }

        // dispatch via strategy
        if ($strategy instanceof ContainerAwareInterface) {
            $strategy->setContainer($this->container);
        }
        if ($middleware) {
            Middleware::run($middleware);
        }

        return $strategy->dispatch($controller, $vars);
    }
}