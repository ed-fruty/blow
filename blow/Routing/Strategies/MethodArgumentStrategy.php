<?php
namespace Blow\Routing\Strategies;

use League\Route\Strategy\MethodArgumentStrategy as ParentStrategy;

class MethodArgumentStrategy extends ParentStrategy
{
    /**
     * {@inheritdoc}
     */
    public function dispatch($controller, array $vars)
    {
        if (is_array($controller)) {
            $controller = [
                $this->container->get($controller[0]),
                $controller[1]
            ];
        }

        $response = $this->container->callAction($controller);

        return $this->determineResponse($response);
    }
}