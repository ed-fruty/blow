<?php
namespace Blow\Routing\Strategies;

use League\Route\Strategy\RequestResponseStrategy as ParentStrategy;
use Symfony\Component\HttpFoundation\Response;

class RequestResponseStrategy extends  ParentStrategy
{
    /**
     * {@inheritdoc}
     */
    public function dispatch($controller, array $vars)
    {
        $response = $this->invokeController($controller, [
            $this->getContainer()->get('request'),
            $this->getContainer()->get('Symfony\Component\HttpFoundation\Response'),
            $vars
        ]);

        if ($response instanceof Response) {
            return $response;
        }

        return new Response($response);
    }
}